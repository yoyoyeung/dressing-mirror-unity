﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Setting : MonoBehaviour {

    public static bool Horizontal;
    public static bool Verticl;
    public static bool Boy;
    public static bool Girl;

    public GameObject Panel1;
    public GameObject Panel2;
    public GameObject Panel3;

    public Image Zoomimage;
    public Image Capimage;
    public Image Coatimage;
    public Image Pantsimage;
    public Image Shoesimage;
    public Image Accesoriesimage;

    public Sprite BoyZoomTexture;
    public Sprite BoyCapTexture;
    public Sprite BoyCoatTexture;
    public Sprite BoyPantsTexture;
    public Sprite BoyShoesTexture;
    public Sprite BoyAccesoriesTexture;

    public Sprite GirlZoomTexture;
    public Sprite GirlCapTexture;
    public Sprite GirlCoatTexture;
    public Sprite GirlPantsTexture;
    public Sprite GirlShoesTexture;
    public Sprite GirlAccesoriesTexture;

    void Start () {
        /*if(Boy == true)
        {
            Zoomimage.sprite = BoyZoomTexture;
            Capimage.sprite = BoyCapTexture;
            Coatimage.sprite = BoyCoatTexture;
            Pantsimage.sprite = BoyPantsTexture;
            Shoesimage.sprite = BoyShoesTexture;
            Accesoriesimage.sprite = BoyAccesoriesTexture;
        }
        else if (Girl == true)
        {
            Zoomimage.sprite = GirlZoomTexture;
            Capimage.sprite = GirlCapTexture;
            Coatimage.sprite = GirlCoatTexture;
            Pantsimage.sprite = GirlPantsTexture;
            Shoesimage.sprite = GirlShoesTexture;
            Accesoriesimage.sprite = GirlAccesoriesTexture;
        }*/
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void HorizontalButton()
    {
        Horizontal = true;
        Verticl = false;
        Panel1.SetActive(false);
        Panel2.SetActive(true);
        //Application.LoadLevel("Setting2");
    }

    public void VerticlButton()
    {
        Horizontal = false;
        Verticl = true;
        Panel1.SetActive(false);
        Panel2.SetActive(true);
        //Application.LoadLevel("Setting2");
    }

    public void BoyButton()
    {
        Boy = true;
        Girl = false;
        if(Horizontal == true)
        {
            Panel2.SetActive(false);
            Panel3.SetActive(true);
        }else if(Verticl == true)
        {
            //Application.LoadLevel("KinectVerticl");
        }
        if (Boy == true)
        {
            Zoomimage.sprite = BoyZoomTexture;
            Capimage.sprite = BoyCapTexture;
            Coatimage.sprite = BoyCoatTexture;
            Pantsimage.sprite = BoyPantsTexture;
            Shoesimage.sprite = BoyShoesTexture;
            Accesoriesimage.sprite = BoyAccesoriesTexture;
        }
    }

    public void GirlButton()
    {
        Boy = false;
        Girl = true;
        if (Horizontal == true)
        {
            Panel2.SetActive(false);
            Panel3.SetActive(true);
            //Application.LoadLevel("KinectHorizontal");
        }
        else if (Verticl == true)
        {
            //Application.LoadLevel("KinectVerticl");
        }
        if (Girl == true)
        {
            Zoomimage.sprite = GirlZoomTexture;
            Capimage.sprite = GirlCapTexture;
            Coatimage.sprite = GirlCoatTexture;
            Pantsimage.sprite = GirlPantsTexture;
            Shoesimage.sprite = GirlShoesTexture;
            Accesoriesimage.sprite = GirlAccesoriesTexture;
        }
    }

    public void BackBGButton()
    {
        //Application.LoadLevel("Setting1");
        Panel2.SetActive(false);
        Panel1.SetActive(true);
    }

    public void BackDressButton()
    {
        //Application.LoadLevel("Setting2");
        Panel3.SetActive(false);
        Panel2.SetActive(true);
    }
}
