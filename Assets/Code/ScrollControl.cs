﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ScrollControl : MonoBehaviour
{
    private Scrollbar scrollbar;
    public float scrollbarvalue;
    private float scrollnowvalue = 0;
    private bool leftused = false;
    private bool rightused = false;
    public int numberOfModels;
    public RectTransform currentRT;

    void Start()
    {
        scrollbar = GetComponent<Scrollbar>();
    }
    public void ListScroll()
    {
        //list.localPosition = new Vector3(list.localPosition.x, scrollbar.value * 380.0f, list.localPosition.z);
        currentRT.localPosition = new Vector3(scrollbar.value * (-40 * numberOfModels), currentRT.localPosition.y, currentRT.localPosition.z);
    }

    public void LeftListUp()
    {
        leftused = false;
    }

    public void LeftListDown()
    {
        leftused = true;
    }

    public void RightListUp()
    {
        rightused = false;
    }

    public void RightListDown()
    {
        rightused = true;
    }

    void Update()
    {
        if (leftused)
        {
            if (scrollnowvalue >= 0.01)
            {
                scrollbarvalue = GetComponent<Scrollbar>().value = scrollnowvalue - 0.01f;
                scrollnowvalue -= 0.01f;
            }
        }
        if (rightused)
        {
            if (scrollnowvalue <= 0.99)
            {
                scrollbarvalue = GetComponent<Scrollbar>().value = scrollnowvalue + 0.01f;
                scrollnowvalue += 0.01f;
            }
        }
    }
}