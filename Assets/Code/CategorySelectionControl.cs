﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CategorySelectionControl : MonoBehaviour {

    public bool isMale;

    public GameObject scrollBar;
    public GameObject boyCoatPanel;
    public GameObject boyPantsPanel;
    public GameObject boyCapPanel;
    public GameObject boyShoesPanel;
    public GameObject boyAccPanel;
    public GameObject girlCoatPanel;
    public GameObject girlPantsPanel;
    public GameObject girlCapPanel;
    public GameObject girlShoesPanel;
    public GameObject girlAccPanel;

    private ModelSelectionControl[] allModelSelectors;
    private ModelSelectionControl modelSelector;
    // Use this for initialization
    void Start () {
        allModelSelectors = new ModelSelectionControl[10];
        allModelSelectors[0] = boyCoatPanel.GetComponent<ModelSelectionControl>();
        allModelSelectors[1] = boyPantsPanel.GetComponent<ModelSelectionControl>();
        allModelSelectors[2] = boyCapPanel.GetComponent<ModelSelectionControl>();
        allModelSelectors[3] = boyShoesPanel.GetComponent<ModelSelectionControl>();
        allModelSelectors[4] = boyAccPanel.GetComponent<ModelSelectionControl>();
        allModelSelectors[5] = girlCoatPanel.GetComponent<ModelSelectionControl>();
        allModelSelectors[6] = girlPantsPanel.GetComponent<ModelSelectionControl>();
        allModelSelectors[7] = girlCapPanel.GetComponent<ModelSelectionControl>();
        allModelSelectors[8] = girlShoesPanel.GetComponent<ModelSelectionControl>();
        allModelSelectors[9] = girlAccPanel.GetComponent<ModelSelectionControl>();
        selectionCategory(isMale ? 1 : 6);

    }

    // Update is called once per frame
    void Update () {
		
	}

    public void selectionCategory(int category)
    {
        if (modelSelector)
        {
            modelSelector.SetActiveSelector(false);
        }
        modelSelector = allModelSelectors[category];
        modelSelector.SetActiveSelector(true);
        ScrollControl sc = scrollBar.GetComponent<ScrollControl>();
        sc.scrollbarvalue = 0.0f;
        sc.currentRT = modelSelector.gameObject.GetComponent<RectTransform>();
        sc.numberOfModels = modelSelector.numberOfModels;
        modelSelector.gameObject.transform.localPosition = new Vector3(-10, 0, 0);

    }
}
