﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeftPanelButtonControl : MonoBehaviour {

    public Button capButton;
    public Button coatButton;
    public Button pantsButtton;
    public Button shoesButton;
    public Button accesoriesButton;
    public GameObject modelsMenu;

    Vector3 capOldPosition;
    Vector3 coatOldPosition;
    Vector3 pantsOldPosition;
    Vector3 shoesOldPosition;
    Vector3 accesoriesOldPosition;

    public int position = 1;
    public int nowposition = 1;
    public int changenum = 0;
    bool shouldHandleClick = true;
    int speed = 500;

    void Start () {
        capButton.onClick.AddListener(onCapClick);
        coatButton.onClick.AddListener(onCoatClick);
        pantsButtton.onClick.AddListener(onPantsClick);
        shoesButton.onClick.AddListener(onShoesClick);
        accesoriesButton.onClick.AddListener(onAccesoriesClick);
    }

    private void onCapClick()
    {
        if (shouldHandleClick)
        {
            CategorySelectionControl csc = modelsMenu.GetComponent<CategorySelectionControl>();
            csc.selectionCategory(2);
            capOldPosition = capButton.transform.position;
            coatOldPosition = coatButton.transform.position;
            pantsOldPosition = pantsButtton.transform.position;
            shoesOldPosition = shoesButton.transform.position;
            accesoriesOldPosition = accesoriesButton.transform.position;
            position = 2;
            Findchangecount();
        }
    }

    private void onCoatClick()
    {
        if (shouldHandleClick)
        {
            CategorySelectionControl csc = modelsMenu.GetComponent<CategorySelectionControl>();
            csc.selectionCategory(0);
            capOldPosition = capButton.transform.position;
            coatOldPosition = coatButton.transform.position;
            pantsOldPosition = pantsButtton.transform.position;
            shoesOldPosition = shoesButton.transform.position;
            accesoriesOldPosition = accesoriesButton.transform.position;
            position = 3;
            Findchangecount();
        }
    }

    private void onPantsClick()
    {
        if (shouldHandleClick)
        {
            CategorySelectionControl csc = modelsMenu.GetComponent<CategorySelectionControl>();
            csc.selectionCategory(1);
            capOldPosition = capButton.transform.position;
            coatOldPosition = coatButton.transform.position;
            pantsOldPosition = pantsButtton.transform.position;
            shoesOldPosition = shoesButton.transform.position;
            accesoriesOldPosition = accesoriesButton.transform.position;
            position = 1;
            Findchangecount();
        }
    }

    private void onShoesClick()
    {
        if (shouldHandleClick)
        {
            CategorySelectionControl csc = modelsMenu.GetComponent<CategorySelectionControl>();
            csc.selectionCategory(3);
            capOldPosition = capButton.transform.position;
            coatOldPosition = coatButton.transform.position;
            pantsOldPosition = pantsButtton.transform.position;
            shoesOldPosition = shoesButton.transform.position;
            accesoriesOldPosition = accesoriesButton.transform.position;
            position = 4;
            Findchangecount();
        }
    }

    private void onAccesoriesClick()
    {
        if (shouldHandleClick)
        {
            CategorySelectionControl csc = modelsMenu.GetComponent<CategorySelectionControl>();
            csc.selectionCategory(4);
            capOldPosition = capButton.transform.position;
            coatOldPosition = coatButton.transform.position;
            pantsOldPosition = pantsButtton.transform.position;
            shoesOldPosition = shoesButton.transform.position;
            accesoriesOldPosition = accesoriesButton.transform.position;
            position = 5;
            Findchangecount();
        }
    }

    private int Findchangecount()
    {
        switch (nowposition)
        {
            case 1:
                if (position == 1) { changenum = 0; }
                if (position == 2) { changenum = 1; }
                if (position == 3) { changenum = 2; }
                if (position == 4) { changenum = 3; }
                if (position == 5) { changenum = 4; }
                nowposition = position;
                break;
            case 2:
                if (position == 1) { changenum = 4; }
                if (position == 2) { changenum = 0; }
                if (position == 3) { changenum = 1; }
                if (position == 4) { changenum = 2; }
                if (position == 5) { changenum = 3; }
                nowposition = position;
                break;
            case 3:
                if (position == 1) { changenum = 3; }
                if (position == 2) { changenum = 4; }
                if (position == 3) { changenum = 0; }
                if (position == 4) { changenum = 1; }
                if (position == 5) { changenum = 2; }
                nowposition = position;
                break;
            case 4:
                if (position == 1) { changenum = 2; }
                if (position == 2) { changenum = 3; }
                if (position == 3) { changenum = 4; }
                if (position == 4) { changenum = 0; }
                if (position == 5) { changenum = 1; }
                nowposition = position;
                break;
            case 5:
                if (position == 1) { changenum = 1; }
                if (position == 2) { changenum = 2; }
                if (position == 3) { changenum = 3; }
                if (position == 4) { changenum = 4; }
                if (position == 5) { changenum = 0; }
                nowposition = position;
                break;
        }
        return 0;
    }

    // Update is called once per frame
    void Update()
    {
        switch (changenum)
        {
            case 1:
                shouldHandleClick = capButton.transform.position == pantsOldPosition;
                break;
            case 2:
                shouldHandleClick = capButton.transform.position == accesoriesOldPosition;
                break;
            case 3:
                shouldHandleClick = capButton.transform.position == shoesOldPosition;
                break;
            case 4:
                shouldHandleClick = capButton.transform.position == coatOldPosition;
                break;
        }

        switch (changenum)
        {
            case 1:
                capButton.transform.position = Vector3.MoveTowards(capButton.transform.position, pantsOldPosition, speed * Time.deltaTime);
                coatButton.transform.position = Vector3.MoveTowards(coatButton.transform.position, capOldPosition, speed * Time.deltaTime);
                pantsButtton.transform.position = Vector3.MoveTowards(pantsButtton.transform.position, accesoriesOldPosition, speed * Time.deltaTime);
                shoesButton.transform.position = Vector3.MoveTowards(shoesButton.transform.position, coatOldPosition, speed * Time.deltaTime);
                accesoriesButton.transform.position = Vector3.MoveTowards(accesoriesButton.transform.position, shoesOldPosition, speed * Time.deltaTime);
                break;
            case 2:
                capButton.transform.position = Vector3.MoveTowards(capButton.transform.position, accesoriesOldPosition, speed * Time.deltaTime);
                coatButton.transform.position = Vector3.MoveTowards(coatButton.transform.position, pantsOldPosition, speed * Time.deltaTime);
                pantsButtton.transform.position = Vector3.MoveTowards(pantsButtton.transform.position, shoesOldPosition, speed * Time.deltaTime);
                shoesButton.transform.position = Vector3.MoveTowards(shoesButton.transform.position, capOldPosition, speed * Time.deltaTime);
                accesoriesButton.transform.position = Vector3.MoveTowards(accesoriesButton.transform.position, coatOldPosition, speed * Time.deltaTime);
                break;
            case 3:
                capButton.transform.position = Vector3.MoveTowards(capButton.transform.position, shoesOldPosition, speed * Time.deltaTime);
                coatButton.transform.position = Vector3.MoveTowards(coatButton.transform.position, accesoriesOldPosition, speed * Time.deltaTime);
                pantsButtton.transform.position = Vector3.MoveTowards(pantsButtton.transform.position, coatOldPosition, speed * Time.deltaTime);
                shoesButton.transform.position = Vector3.MoveTowards(shoesButton.transform.position, pantsOldPosition, speed * Time.deltaTime);
                accesoriesButton.transform.position = Vector3.MoveTowards(accesoriesButton.transform.position, capOldPosition, speed * Time.deltaTime);
                break;
            case 4:
                capButton.transform.position = Vector3.MoveTowards(capButton.transform.position, coatOldPosition, speed * Time.deltaTime);
                coatButton.transform.position = Vector3.MoveTowards(coatButton.transform.position, shoesOldPosition, speed * Time.deltaTime);
                pantsButtton.transform.position = Vector3.MoveTowards(pantsButtton.transform.position, capOldPosition, speed * Time.deltaTime);
                shoesButton.transform.position = Vector3.MoveTowards(shoesButton.transform.position, accesoriesOldPosition, speed * Time.deltaTime);
                accesoriesButton.transform.position = Vector3.MoveTowards(accesoriesButton.transform.position, pantsOldPosition, speed * Time.deltaTime);
                break;
        }
    }
}
