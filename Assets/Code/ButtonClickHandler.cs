﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClickHandler : MonoBehaviour, InteractionListenerInterface
{
    public bool isHorizontal;
    public bool isMale;

    public GameObject curser;
    public GameObject Panel1;
    public GameObject Panel2;
    public GameObject Panel3;
    public GameObject modelsMenu;

    public Button HorizontalButton;
    public Button VerticlButton;
    public Button BoyButton;
    public Button GirlButton;
    public Button BackBGButton;
    public Button BackDressButton;
    public Button capButton;
    public Button coatButton;
    public Button pantsButton;
    public Button shoesButton;
    public Button accesoriesButton;

    Vector3 capOldPosition;
    Vector3 coatOldPosition;
    Vector3 pantsOldPosition;
    Vector3 shoesOldPosition;
    Vector3 accesoriesOldPosition;

    public int position = 1;
    public int nowposition = 1;
    public int changenum = 0;
    bool shouldHandleClick = true;
    int speed = 200;

    public Image Zoomimage;
    public Image Capimage;
    public Image Coatimage;
    public Image Pantsimage;
    public Image Shoesimage;
    public Image Accesoriesimage;

    public Sprite BoyZoomTexture;
    public Sprite BoyCapTexture;
    public Sprite BoyCoatTexture;
    public Sprite BoyPantsTexture;
    public Sprite BoyShoesTexture;
    public Sprite BoyAccesoriesTexture;

    public Sprite GirlZoomTexture;
    public Sprite GirlCapTexture;
    public Sprite GirlCoatTexture;
    public Sprite GirlPantsTexture;
    public Sprite GirlShoesTexture;
    public Sprite GirlAccesoriesTexture;

    private List<Button> modelButtons = new List<Button>();

    public void clickCoat()
    {
        if (shouldHandleClick)
        {
            shouldHandleClick = false;
            CategorySelectionControl csc = modelsMenu.GetComponent<CategorySelectionControl>();
            csc.selectionCategory(isMale ? 0 : 5);
            capOldPosition = capButton.transform.localPosition;
            coatOldPosition = coatButton.transform.localPosition;
            pantsOldPosition = pantsButton.transform.localPosition;
            shoesOldPosition = shoesButton.transform.localPosition;
            accesoriesOldPosition = accesoriesButton.transform.localPosition;
            position = 3;
            Findchangecount();
        }
    }

    public void clickPants()
    {
        if (shouldHandleClick)
        {
            shouldHandleClick = false;
            CategorySelectionControl csc = modelsMenu.GetComponent<CategorySelectionControl>();
            csc.selectionCategory(isMale ? 1 : 6);
            capOldPosition = capButton.transform.localPosition;
            coatOldPosition = coatButton.transform.localPosition;
            pantsOldPosition = pantsButton.transform.localPosition;
            shoesOldPosition = shoesButton.transform.localPosition;
            accesoriesOldPosition = accesoriesButton.transform.localPosition;
            position = 1;
            Findchangecount();
        }
    }

    public void clickCap()
    {
        if (shouldHandleClick)
        {
            shouldHandleClick = false;
            CategorySelectionControl csc = modelsMenu.GetComponent<CategorySelectionControl>();
            csc.selectionCategory(isMale ? 2 : 7);
            capOldPosition = capButton.transform.localPosition;
            coatOldPosition = coatButton.transform.localPosition;
            pantsOldPosition = pantsButton.transform.localPosition;
            shoesOldPosition = shoesButton.transform.localPosition;
            accesoriesOldPosition = accesoriesButton.transform.localPosition;
            position = 2;
            Findchangecount();
        }
    }

    public void clickShoes()
    {
        if (shouldHandleClick)
        {
            shouldHandleClick = false;
            CategorySelectionControl csc = modelsMenu.GetComponent<CategorySelectionControl>();
            csc.selectionCategory(isMale ? 3 : 8);
            capOldPosition = capButton.transform.localPosition;
            coatOldPosition = coatButton.transform.localPosition;
            pantsOldPosition = pantsButton.transform.localPosition;
            shoesOldPosition = shoesButton.transform.localPosition;
            accesoriesOldPosition = accesoriesButton.transform.localPosition;
            position = 4;
            Findchangecount();
        }
    }

    public void clickAcc()
    {
        if (shouldHandleClick)
        {
            shouldHandleClick = false;
            CategorySelectionControl csc = modelsMenu.GetComponent<CategorySelectionControl>();
            csc.selectionCategory(isMale ? 4 : 9);
            capOldPosition = capButton.transform.localPosition;
            coatOldPosition = coatButton.transform.localPosition;
            pantsOldPosition = pantsButton.transform.localPosition;
            shoesOldPosition = shoesButton.transform.localPosition;
            accesoriesOldPosition = accesoriesButton.transform.localPosition;
            position = 5;
            Findchangecount();
        }
    }

    public void clickHorizontal()
    {
        isHorizontal = true;
        Panel1.SetActive(false);
        Panel2.SetActive(true);
    }

    public void clickVertical()
    {
        isHorizontal = false;
        Panel1.SetActive(false);
        Panel2.SetActive(true);
    }

    public void clickMale()
    {
        isMale = true;
        if (isHorizontal == true)
        {
            Panel2.SetActive(false);
            Panel3.SetActive(true);
        }
        else
        {
            Panel2.SetActive(false);
            Panel3.SetActive(true);
        }
        Zoomimage.sprite = BoyZoomTexture;
        Capimage.sprite = BoyCapTexture;
        Coatimage.sprite = BoyCoatTexture;
        Pantsimage.sprite = BoyPantsTexture;
        Shoesimage.sprite = BoyShoesTexture;
        Accesoriesimage.sprite = BoyAccesoriesTexture;
        modelsMenu.GetComponent<CategorySelectionControl>().isMale = true;
    }

    public void clickFemale()
    {
        isMale = false;
        if (isHorizontal == true)
        {
            Panel2.SetActive(false);
            Panel3.SetActive(true);
        }
        else
        {
            Panel2.SetActive(false);
            Panel3.SetActive(true);
        }
        Zoomimage.sprite = GirlZoomTexture;
        Capimage.sprite = GirlCapTexture;
        Coatimage.sprite = GirlCoatTexture;
        Pantsimage.sprite = GirlPantsTexture;
        Shoesimage.sprite = GirlShoesTexture;
        Accesoriesimage.sprite = GirlAccesoriesTexture;
        modelsMenu.GetComponent<CategorySelectionControl>().isMale = false;
    }

    public void clickBackToOrientation()
    {
        Panel2.SetActive(false);
        Panel1.SetActive(true);
    }

    public void clickBackToGender()
    {
        Panel3.SetActive(false);
        Panel2.SetActive(true);
    }

    private void clickOrGrip()
    {
        if (isCurseWithinButton(HorizontalButton) && Panel1.activeSelf == true)
        {
            clickHorizontal();
        }
        else if (isCurseWithinButton(VerticlButton) && Panel1.activeSelf == true)
        {
            clickVertical();
        }
        else if (isCurseWithinButton(BoyButton) && Panel2.activeSelf == true)
        {
            clickMale();
        }
        else if (isCurseWithinButton(GirlButton) && Panel2.activeSelf == true)
        {
            clickFemale();
        }
        else if (isCurseWithinButton(BackBGButton) && Panel2.activeSelf == true)
        {
            clickBackToOrientation();
        }
        else if (isCurseWithinButton(BackDressButton) && Panel3.activeSelf == true)
        {
            clickBackToGender();
        }
        else if (isCurseWithinButton(capButton) && Panel3.activeSelf == true)
        {
            clickCap();
        }
        else if (isCurseWithinButton(coatButton) && Panel3.activeSelf == true)
        {
            clickCoat();
        }
        else if (isCurseWithinButton(pantsButton) && Panel3.activeSelf == true)
        {
            clickPants();
        }
        else if (isCurseWithinButton(shoesButton) && Panel3.activeSelf == true)
        {
            clickShoes();
        }
        else if (isCurseWithinButton(accesoriesButton) && Panel3.activeSelf == true)
        {
            clickAcc();
        }
        /*
        for (int i = 0; i < modelButtons.Count; i++)
        {
            if (isCurseWithinButton(modelButtons[i]) && Panel3.activeSelf)
            {
                FindObjectOfType<ModelSelectionControl>().OnDressingItemSelected(i);
            }
        }
        */
    }
    public void HandGripDetected(long userId, int userIndex, bool isRightHand, bool isHandInteracting, Vector3 handScreenPos)
    {
        clickOrGrip();
    }

    public bool isCurseWithinButton(Button btn)
    {
        RectTransform rt = btn.GetComponent<RectTransform>();
        Vector3 curserPos = curser.transform.position;
        float w = rt.rect.width / 2;
        float h = rt.rect.height / 2;
        if (curserPos.x > (rt.position.x - h)
            && curserPos.x < (rt.position.x + h)
            && curserPos.y > (rt.position.y - w)
            && curserPos.y < (rt.position.y + w))
        {
            return true;
        }
        return false;
    }

    public void HandReleaseDetected(long userId, int userIndex, bool isRightHand, bool isHandInteracting, Vector3 handScreenPos)
    {

    }

    public bool HandClickDetected(long userId, int userIndex, bool isRightHand, Vector3 handScreenPos)
    {
        clickOrGrip();
        return true;
    }
    // Use this for initialization
    void Start () {
        coatButton.onClick.AddListener(clickCoat);
        pantsButton.onClick.AddListener(clickPants);
        capButton.onClick.AddListener(clickCap);
        shoesButton.onClick.AddListener(clickShoes);
        accesoriesButton.onClick.AddListener(clickAcc);
	}

    private int Findchangecount()
    {
        switch (nowposition)
        {
            case 1:
                if (position == 1) { changenum = 0; }
                if (position == 2) { changenum = 1; }
                if (position == 3) { changenum = 2; }
                if (position == 4) { changenum = 3; }
                if (position == 5) { changenum = 4; }
                nowposition = position;
                break;
            case 2:
                if (position == 1) { changenum = 4; }
                if (position == 2) { changenum = 0; }
                if (position == 3) { changenum = 1; }
                if (position == 4) { changenum = 2; }
                if (position == 5) { changenum = 3; }
                nowposition = position;
                break;
            case 3:
                if (position == 1) { changenum = 3; }
                if (position == 2) { changenum = 4; }
                if (position == 3) { changenum = 0; }
                if (position == 4) { changenum = 1; }
                if (position == 5) { changenum = 2; }
                nowposition = position;
                break;
            case 4:
                if (position == 1) { changenum = 2; }
                if (position == 2) { changenum = 3; }
                if (position == 3) { changenum = 4; }
                if (position == 4) { changenum = 0; }
                if (position == 5) { changenum = 1; }
                nowposition = position;
                break;
            case 5:
                if (position == 1) { changenum = 1; }
                if (position == 2) { changenum = 2; }
                if (position == 3) { changenum = 3; }
                if (position == 4) { changenum = 4; }
                if (position == 5) { changenum = 0; }
                nowposition = position;
                break;
        }
        return 0;
    }

    private int disableClickCount = 0;
    void Update () {

        ModelSelectionControl msc = FindObjectOfType<ModelSelectionControl>();
        if (msc)
        {
            modelButtons.Clear();
            foreach (Transform t in msc.modelMenu.transform)
            {
                modelButtons.Add(t.gameObject.GetComponent<Button>());
            }
        }

        if (!shouldHandleClick)
        {
            disableClickCount++;
            if (disableClickCount >= 30)
            {
                shouldHandleClick = true;
                disableClickCount = 0;
            }
        }
        switch (changenum)
        {
            case 1:
                capButton.transform.localPosition = Vector3.MoveTowards(capButton.transform.localPosition, pantsOldPosition, speed * Time.deltaTime);
                coatButton.transform.localPosition = Vector3.MoveTowards(coatButton.transform.localPosition, capOldPosition, speed * Time.deltaTime);
                pantsButton.transform.localPosition = Vector3.MoveTowards(pantsButton.transform.localPosition, accesoriesOldPosition, speed * Time.deltaTime);
                shoesButton.transform.localPosition = Vector3.MoveTowards(shoesButton.transform.localPosition, coatOldPosition, speed * Time.deltaTime);
                accesoriesButton.transform.localPosition = Vector3.MoveTowards(accesoriesButton.transform.localPosition, shoesOldPosition, speed * Time.deltaTime);
                break;
            case 2:
                capButton.transform.localPosition = Vector3.MoveTowards(capButton.transform.localPosition, accesoriesOldPosition, speed * Time.deltaTime);
                coatButton.transform.localPosition = Vector3.MoveTowards(coatButton.transform.localPosition, pantsOldPosition, speed * Time.deltaTime);
                pantsButton.transform.localPosition = Vector3.MoveTowards(pantsButton.transform.localPosition, shoesOldPosition, speed * Time.deltaTime);
                shoesButton.transform.localPosition = Vector3.MoveTowards(shoesButton.transform.localPosition, capOldPosition, speed * Time.deltaTime);
                accesoriesButton.transform.localPosition = Vector3.MoveTowards(accesoriesButton.transform.localPosition, coatOldPosition, speed * Time.deltaTime);
                break;
            case 3:
                capButton.transform.localPosition = Vector3.MoveTowards(capButton.transform.localPosition, shoesOldPosition, speed * Time.deltaTime);
                coatButton.transform.localPosition = Vector3.MoveTowards(coatButton.transform.localPosition, accesoriesOldPosition, speed * Time.deltaTime);
                pantsButton.transform.localPosition = Vector3.MoveTowards(pantsButton.transform.localPosition, coatOldPosition, speed * Time.deltaTime);
                shoesButton.transform.localPosition = Vector3.MoveTowards(shoesButton.transform.localPosition, pantsOldPosition, speed * Time.deltaTime);
                accesoriesButton.transform.localPosition = Vector3.MoveTowards(accesoriesButton.transform.localPosition, capOldPosition, speed * Time.deltaTime);
                break;
            case 4:
                capButton.transform.localPosition = Vector3.MoveTowards(capButton.transform.localPosition, coatOldPosition, speed * Time.deltaTime);
                coatButton.transform.localPosition = Vector3.MoveTowards(coatButton.transform.localPosition, shoesOldPosition, speed * Time.deltaTime);
                pantsButton.transform.localPosition = Vector3.MoveTowards(pantsButton.transform.localPosition, capOldPosition, speed * Time.deltaTime);
                shoesButton.transform.localPosition = Vector3.MoveTowards(shoesButton.transform.localPosition, accesoriesOldPosition, speed * Time.deltaTime);
                accesoriesButton.transform.localPosition = Vector3.MoveTowards(accesoriesButton.transform.localPosition, pantsOldPosition, speed * Time.deltaTime);
                break;
        }
    }
}
