﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelFollowUserControl : MonoBehaviour {

    public GameObject modelToFollow;
	// Use this for initialization
	void Start () {
    }

    // Update is called once per frame
    void Update () {

        if (modelToFollow)
        {
            Vector3 targetPosition = Camera.main.WorldToScreenPoint(modelToFollow.transform.position);
            transform.position = targetPosition;
        }

	}
}
